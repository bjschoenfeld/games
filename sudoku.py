from random import shuffle
import time

class Sudoku():
	
	def __init__(self, puzzle):
		# to maintain a clean copy of the original puzzle
		self.puzzle = [[num for num in row] for row in puzzle]
		# a copy of the puzzle for solving
		self.solution = [[num for num in row] for row in puzzle]
		# time limit and increment amounts for recursion
		self.rec_time_limit = .05
		self.rec_time_inc = .01
	
	'''
	def __validate(self):
		for r in xrange(9):
			for c in xrange(9):
				if self.solution[r][c] == 0:
					return False
		return True
	'''
	
	def solve(self, verbose=False):
		'''
		wrapper for the recursive function which solves the puzzle
		returns True if a solution was found, False otherwise
		will start recursion over if the time limit is exceeded
		'''
		# recursion constants
		TIME_OUT = -1
		INVALID = 0
		VALID = 1

		def recurse():
			'''
			another layer of wrapping to limit the time on the recursion

			'''
			# valid numbers in sudoku puzzle
			default_values = range(1,10)
			# randomized order to minimize worst-case scenarios
			shuffle(default_values)

			
			def get_valid_values(row, col):
				'''
				finds and returns a list of the valid values for the given position in the puzzle

				'''
				invalid_values = set()

				# check current row and column
				for i in xrange(9):
					invalid_values.add(self.solution[row][i])
					invalid_values.add(self.solution[i][col])

				# check current box
				box_row, box_col = (row / 3) * 3, (col / 3) * 3		# ineger division and multiplication maps [0,1,2]->0, [3,4,5]->3, [6,7,8]->6
				for r in xrange(box_row, box_row+3):
					for c in xrange(box_col, box_col+3):
						invalid_values.add(self.solution[r][c])

				# set difference between default_values and invalid_values in a list
				valid_values = [v for v in default_values if v not in invalid_values]
				return valid_values

		
			
			out_of_time = False
			def timeout():
				out_of_time = True
			def r_solve(p, start=time.time()):
				'''
				recursive function to solve the puzzle
				p is the combined index of row and column for convenience
				'''
				# base case, problem solved
				if p == 81:
					return VALID

				if time.time() - start > self.rec_time_limit:
					timeout()
					if verbose:
						print "Timed out"
					return TIME_OUT

				# separate p into row and column
				row = p / 9
				col = p % 9

				# check if current position already has a value
				if self.solution[row][col] != 0:
					if out_of_time:
						return TIME_OUT
					return r_solve(p+1, start)

				# try all valid values at this location
				else:
					possible_values = get_valid_values(row, col)
					# try all possible values; note that if there aren't any, this loop will break immediately
					for v in possible_values:
						# try the current value v and recurse
						self.solution[row][col] = v
						if out_of_time:
							self.solution[row][col] = 0
							return TIME_OUT
						result = r_solve(p+1, start)
						# if v did not work, set the current location back to 0, the default "empty square"
						if result == TIME_OUT:
							self.solution[row][col] = 0
							return TIME_OUT
						elif result == INVALID:
							self.solution[row][col] = 0
						# if v reached a valid solution
						else:
							return VALID
					# all the possible values (if any) did not work
					return INVALID
			
			# run the recursion starting at index 0
			return r_solve(0)

		# keep trying the recursion
		result = recurse()
		while result == TIME_OUT:
			self.rec_time_limit += self.rec_time_inc
			result = recurse()

		# return whether the solution is valid
		return result == VALID
			
	def print_puzzle(self):
		# writes the puzzle to standard out
		for r in self.puzzle:
			print r
			
	def print_solution(self):
		# writes the solution to standard out
		# if solve has not been run or did not find a valid solution, this will write the original puzzle
		for r in self.solution:
			print r


if __name__ == "__main__":
	'''
	test cases
	'''
# valid puzzle found online
valid = [[0,0,0,5,0,7,0,0,0],
		 [0,0,2,4,0,6,3,0,0],
		 [0,9,0,0,1,0,0,2,0],
		 [2,7,0,0,0,0,0,6,8],
		 [0,0,3,0,0,0,1,0,0],
		 [1,4,0,0,0,0,0,9,3],
		 [0,6,0,0,4,0,0,5,0],
		 [0,0,9,2,0,5,6,0,0],
		 [0,0,0,9,0,3,0,0,0]]

# a blank sudoku. this is technically not a "valid" puzzle, but a solution should be found quickly
empty = [[0] *9 for i in xrange(9)]

# worst case scenario, should cause a timeout if 1 comes before 9 in the random ordering of possible values
evil = [[0]*9 for i in xrange(9)]
for i in xrange(2,9):
	evil[i-1][0] = i
evil[8][8] = 9

# testing each puzzle
puzzles = [valid, empty, evil]

for p in puzzles:
	s = Sudoku(p)
	start_time = time.time()
	s.solve(verbose=True)
	print "actual time to solution: ", time.time() - start_time
	s.print_solution()
