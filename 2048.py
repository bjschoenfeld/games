from numpy import uint64, uint16
from numpy.random import randint, rand

'''
This is an AI for the game 2048.

Since the board is a 4x4 grid and the tiles are all powers of 2,
the 16 tiles can be represented with the exponent on the 2.
Since the largest two tiles have never been acheived before,
the max tile is 2^15, which can be represented with 15 or 4 bits.
This gives us 16x4 bits or 64 bits. Thus we represent the board
with a single 64 bit integer! All of the operations assume this.
'''

# global masks
M_ffff = uint64(0xffff)
M_f = uint64(0xf)

#global shifts
S_48 = uint64(48)
S_32 = uint64(32)
S_16 = uint64(16)
S_12 = uint64(12)
S_8 = uint64(8)
S_4 = uint64(4)


def place_random(board, p=.9):
    '''
    assumes that a piece can be placed on the board
    otherwise will cause an infinite loop
    p is the probability of a 2 (vs a 4)
    '''
    while True:
        # select a random tile
        shift = uint64(randint(16) * 4)
        tile = (board >> shift) & M_f
        if tile == 0:
            if rand() < p:     # place a 2 with probability p
                return board | uint64(1) << shift
            else:                               # places a 4 with probability 1-p
                return board | uint64(2) << shift


def init_board():
    '''
    creates a unit64 which represents a 2048 board
    adds two random 2 tiles
    '''
    board = place_random(uint64(0), 1)
    return place_random(board, 1)


def transpose_board(board):
    '''
    static inline board_t transpose(board_t x)
    {
        board_t a1 = x & 0xF0F00F0FF0F00F0FULL;
        board_t a2 = x & 0x0000F0F00000F0F0ULL;
        board_t a3 = x & 0x0F0F00000F0F0000ULL;
        board_t a = a1 | (a2 << 12) | (a3 >> 12);
        board_t b1 = a & 0xFF00FF0000FF00FFULL;
        board_t b2 = a & 0x00FF00FF00000000ULL;
        board_t b3 = a & 0x00000000FF00FF00ULL;
        return b1 | (b2 >> 24) | (b3 << 24);
    }
    '''
    '''
    This transposes the board (the 64 bit integer).
    The magic constants extract specific tiles from the board
    '''
    
    # First we transpose the upper half of the board
    x1 = board &  uint64(0x000f000000000000)
    x1 >>= S_12
    x1 |= board & uint64(0x00f0000f00000000)
    x1 >>= S_12
    x1 |= board & uint64(0x0f0000f0000f0000)
    x1 >>= S_12
    x1 |= board & uint64(0xf0000f0000f0000f)
    
    # Then the lower half
    x2 = board & uint64(0x000000000000f000)
    x2 <<= S_12
    x2 |= board & uint64(0x00000000f0000f00)
    x2 <<= S_12
    x2 |= board & uint64(0x0000f0000f0000f0)
    x2 <<= S_12
    
    return uint64(x1 | x2)

def uint16_to_list(number):
    '''
    assumes a 16 bit integer, otherwise this will not work
    returns [bbbb, bbbb, bbbb, bbbb] where b is a bit and the order 
    is preserved with the highest 4 bits at the lowest index of the list
    '''
    return [number >> S_12, (number & uint64(0x0f00)) >> S_8, (number & uint64(0x00f0)) >> S_4, number & uint64(0x000f)]

def list_to_uint16(l):
    '''
    assumes a list of (at least) size 4 with 4 bit integers
    returns an integer bbbbbbbbbbbbbbbb where b is a bit and the order
    of the bits in the list is preserved
    ignores any elements in the list beyond index 3
    '''
    return uint16((uint16(l[0]) << S_12) | (uint16(l[1]) << S_8) | (uint16(l[2]) << S_4) | uint16(l[3]))

def transpose_row(row):
    return uint16((row >> S_12) | ((row & uint16(0x0f00)) >> S_4) | ((row & uint16(0x00f0)) << S_4) | ((row & M_f) << S_12))


def board_to_string(board):
    digits = uint16_to_list(uint16(board >> uint64(48)))
    digits.extend(uint16_to_list(uint16((board & uint64(0x0000ffff00000000)) >> uint64(32))))
    digits.extend(uint16_to_list(uint16((board & uint64(0x00000000ffff0000)) >> uint64(16))))
    digits.extend(uint16_to_list(uint16(board & uint64(0x000000000000ffff))))
    digits = map(lambda x: 2**x if x else 0, digits)
    return '\n'.join(','.join(str(uint64(digits[i + 4*j])) for i in xrange(4)) for j in xrange(4))

def move_row_left(row):
    nonzero_tiles = filter(lambda x: x, uint16_to_list(uint16(row)))
    for i in xrange(1, len(nonzero_tiles)):
        if nonzero_tiles[i] == nonzero_tiles[i-1]:
            nonzero_tiles[i-1] += 1
            nonzero_tiles[i] = 0
    nonzero_tiles = filter(lambda x: x, nonzero_tiles)
    return list_to_uint16(nonzero_tiles + [0,0,0,0])

def get_move_dictionary():
    dic = {"left":{}, "right":{}}        
    for i in xrange(M_ffff):
        dic["left"][i] = move_row_left(uint16(i))
        dic["right"][i] = transpose_row(move_row_left(transpose_row(uint16(i))))
    return dic

def get_human_move(board, valid_moves=set(["w", "a", "s", "d"]), moves_map={"w":0, "a":1, "s":2, "d":3}):
    '''
    prompts user for move using standard in
    defaults to wasd controls
    returns 0, 1, 2, or 3 correspinding to up, left, down, right resp.
    '''
    print board_to_string(board)
    move = None
    while move not in valid_moves:
        move = raw_input().strip()
    return moves_map[move]

def make_move(board, move, move_dic):
    '''
    move is an int in [0,1,2,3] correspinding to up, left, down, right resp.
    returns updated board
    '''
    if move == 0: # done
        board = transpose_board(board)
        r3 = move_dic["left"][board & M_ffff]
        r2 = move_dic["left"][(board >> S_16) & M_ffff]
        r1 = move_dic["left"][(board >> S_32) & M_ffff]
        r0 = move_dic["left"][(board >> S_48) & M_ffff]
        return transpose_board(uint64( (r0 << S_48) | (r1 << S_32) | (r2 << S_16) | r3))
    if move == 1: # done
        r3 = move_dic["left"][board & M_ffff]
        r2 = move_dic["left"][(board >> S_16) & M_ffff]
        r1 = move_dic["left"][(board >> S_32) & M_ffff]
        r0 = move_dic["left"][(board >> S_48) & M_ffff]
        return uint64( (r0 << S_48) | (r1 << S_32) | (r2 << S_16) | r3)
    if move == 2: # done
        board = transpose_board(board)
        r3 = move_dic["right"][board & M_ffff]
        r2 = move_dic["right"][(board >> S_16) & M_ffff]
        r1 = move_dic["right"][(board >> S_32) & M_ffff]
        r0 = move_dic["right"][(board >> S_48) & M_ffff]
        return transpose_board(uint64( (r0 << S_48) | (r1 << S_32) | (r2 << S_16) | r3))
    if move == 3:
        r3 = move_dic["right"][board & M_ffff]
        r2 = move_dic["right"][(board >> S_16) & M_ffff]
        r1 = move_dic["right"][(board >> S_32) & M_ffff]
        r0 = move_dic["right"][(board >> S_48) & M_ffff]
        return uint64( (r0 << S_48) | (r1 << S_32) | (r2 << S_16) | r3)

def get_valid_moves(board, move_dic):
    valid_moves = []
    for i in xrange(4):
        if board != make_move(board, i, move_dic):
            valid_moves.append(i)
    if len(valid_moves) == 0:
        return None
    return valid_moves

def get_score(board):
    score = 0
    for i in xrange(16):
        score += int(2**(board >> uint64(i*4)) & M_f))
    return score

def play():
    move_dic = get_move_dictionary()
    board = init_board()
    valid_moves = get_valid_moves(board, move_dic)
    while valid_moves:
        move = get_human_move(board)
        while move not in valid_moves:
            print "Not a valid move\n"
            move = get_human_move(board)
        new_board = make_move(board, move, move_dic)
        board = place_random(new_board)
        valid_moves = get_valid_moves(board, move_dic)
    print board_to_string(board)

def eval_corner(board):
    evaluation = 0
    values = [32, 16, 8, 4]
    for i in xrange(15, -1, -1):
        if i > 0 and i%4 == 0:
            values = map(values, lambda x: x//2)
        evaluation += int(2**(board >> uint64(i*4)) * values[i%4])
    return evaluation

def eval_snake(board):
    evaluation = 0
    values = [32, 16, 8, 4]
    for i in xrange(15, -1, -1):
        if i > 0 and i%4 == 0:
            values = map(values, lambda x: x//2)
        evaluation += int(2**(board >> uint64(i*4)) * values[i%4])
    return evaluation

def get_blank_positions(board):
    for i in xrange(15, -1, -1):
        if (board >> uint64(i*4)) & M_f == 0:
            yield uint64(1) << uint64(i*4)
            
def evaluate(board):
    return len(list(get_blank_positions(board)))

         
def get_AI_move(board, depth, move_dic):

    def choose_max(board, depth):
        if depth == 0:
            return evaluate(board)
        else:
            best = float("-inf")
            for move in get_valid_moves(board, move_dic):
                new_board = make_move(board, move, move_dic)
                best = max(choose_average(board, depth-1), best)
            return best
    
    def choose_average(board, depth, p=.9):
        if depth == 0:
            return evaluate(board)
        else:
            total = 0
            n = 0.0
            for b in get_blank_positions(board):
                n += 1
                # 2 tile
                total += choose_max(board | b, depth-1) * p
                # 4 tile
                total += choose_max(board | uint64(2*b), depth-1) * (1-p)
            if n == 0:
                return -1
            return total / n
            
    best = float("-inf")
    best_move = -1
    for move in get_valid_moves(board, move_dic):
        cur = choose_max(make_move(board, move, move_dic), depth)
        if cur > best:
            best = cur
            best_move = move
    return best_move

def play_AI():
    move_dic = get_move_dictionary()
    board = init_board()
    valid_moves = get_valid_moves(board, move_dic)
    while valid_moves:
        move = get_AI_move(board, 4, move_dic)
        new_board = make_move(board, move, move_dic)
        print board_to_string(new_board), "\n"
        board = place_random(new_board)
        valid_moves = get_valid_moves(board, move_dic)
    print board_to_string(board)
    
    
    
    
    
    
